package com.example.demo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.demo.adapter.MyAdapter
import com.example.demo.viewmodel.MainVM
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var mainVM: MainVM
    lateinit var adapter: MyAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainVM = ViewModelProvider(this, MainVM.MainVMFactory(application)).get(MainVM::class.java)
        var linearLayoutManager = LinearLayoutManager(this)
        recycler_view.layoutManager = linearLayoutManager
        adapter = MyAdapter()
        recycler_view.adapter = adapter
        filter_btn.setOnClickListener {
            mainVM.refreshData()
            filter_btn.text = mainVM.list.get(mainVM.selecteId)
        }
        observLiveData()
    }

    private fun observLiveData() {
        mainVM.stockslist.observe(this, Observer {
            adapter.setData(it)
            if (mainVM.selecteId == 0)
                adapter.isPercentShow = true
            else
                adapter.isPercentShow = false
        })
    }
}