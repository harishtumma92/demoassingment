package com.example.demo

import android.graphics.Color
import android.graphics.DashPathEffect
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.demo.adapter.MyMarkerAdapter
import com.example.demo.adapter.ToolTips
import com.example.demo.rawjson.Jsons
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import kotlinx.android.synthetic.main.activity_graph.*
import org.json.JSONArray
import org.json.JSONObject
import timber.log.Timber


class GraphActivity : AppCompatActivity() {
    lateinit var dailyResponse: JSONArray
    lateinit var weeklyResponse: JSONArray
    lateinit var monthlyResponse: JSONArray
    lateinit var yearlyResponse: JSONArray
    lateinit var yearly3Response: JSONArray
    var dailyentries = ArrayList<Entry>()
    var weeklyentries = ArrayList<Entry>()
    var monthlyentries = ArrayList<Entry>()
    var yearlyentries = ArrayList<Entry>()
    var yrarly3entries = ArrayList<Entry>()
    var dailyTooltips = ArrayList<ToolTips>()
    var weeklyTooltips = ArrayList<ToolTips>()
    var monthlyTooltips = ArrayList<ToolTips>()
    var yearlyTooltips = ArrayList<ToolTips>()
    var yearly3Tooltips = ArrayList<ToolTips>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_graph)
        try {
            parsedData()
        } catch (e: Exception) {
            Timber.e(e.toString())
        }


        day_txt.setOnClickListener {
            heighLightSelectedType(day_txt)
            displayChart(dailyentries, dailyTooltips, true)
        }
        week_txt.setOnClickListener {
            heighLightSelectedType(week_txt)
            displayChart(weeklyentries, weeklyTooltips, false)
        }
        month_txt.setOnClickListener {
            heighLightSelectedType(month_txt)
            displayChart(monthlyentries, monthlyTooltips, false)
        }
        year_txt.setOnClickListener {
            heighLightSelectedType(year_txt)
            displayChart(yearlyentries, yearlyTooltips, false)
        }
        year3_txt.setOnClickListener {
            heighLightSelectedType(year3_txt)
            displayChart(yrarly3entries, yearly3Tooltips, false)
        }

        day_txt.performClick()

    }

    private fun displayChart(
        entry: ArrayList<Entry>,
        toolTips: List<ToolTips>,
        isShowTimeOnly: Boolean
    ) {
        var entries = entry
        var dataSet = LineDataSet(entries, "Label"); // add entries to dataset

        // draw dashed line
        dataSet.enableDashedLine(10f, 5f, 0f)

        // black lines and points
        dataSet.setColor(Color.BLACK)
        dataSet.setDrawCircles(false)

        // customize legend entry
        dataSet.setFormLineWidth(1f)
        dataSet.setFormLineDashEffect(DashPathEffect(floatArrayOf(10f, 5f), 0f))
        dataSet.setFormSize(15f)

        val lineData = LineData(dataSet)
        chart.setData(lineData)
        /*      // // X-Axis Style // //
              var xAxis = chart.xAxis
              xAxis.disableAxisLineDashedLine()
              xAxis.disableGridDashedLine()

              var yAxis = chart.axisLeft
              yAxis.disableAxisLineDashedLine()
              yAxis.disableGridDashedLine()*/

        var xAxis: XAxis
        // {   // // X-Axis Style // //
        xAxis = chart.xAxis
        xAxis.setDrawGridLines(false)
        // vertical grid lines
        // xAxis.enableGridDashedLine(10f, 10f, 0f)
        // }

        var yAxis: YAxis
        //{   // // Y-Axis Style // //
        yAxis = chart.axisLeft

        // disable dual axis (only use LEFT axis)
        chart.axisRight.isEnabled = false
        yAxis.setDrawGridLines(false)

        chart.marker = MyMarkerAdapter(this, R.layout.marker_layout, toolTips, isShowTimeOnly)
        chart.invalidate()
    }

    private fun parsedData() {
        dailyResponse = fromJson(Jsons.dailyCandle)
        Timber.i("dailyResponse : +${dailyResponse.toString()}")
        var dailyPair = parseEntryList(dailyResponse)
        dailyentries = dailyPair.first
        dailyTooltips = dailyPair.second

        weeklyResponse = fromJson(Jsons.weeklyCandle)
        Timber.i("weeklyResponse : +${weeklyResponse.toString()}")
        var weeklyPair = parseEntryList(weeklyResponse)
        weeklyentries = weeklyPair.first
        weeklyTooltips = weeklyPair.second

        monthlyResponse = fromJson(Jsons.monthlyCandle)
        Timber.i("monthlyResponse : +${monthlyResponse.toString()}")
        var monthlyPair = parseEntryList(monthlyResponse)
        monthlyentries = monthlyPair.first
        monthlyTooltips = monthlyPair.second

        yearlyResponse = fromJson(Jsons.yearlyCandle)
        Timber.i("yearlyResponse : +${yearlyResponse.toString()}")
        var yearlyPair = parseEntryList(yearlyResponse)
        yearlyentries = yearlyPair.first
        yearlyTooltips = yearlyPair.second

        yearly3Response = fromJson(Jsons.yearly3Candle)
        Timber.i("yearly3Response : +${yearly3Response.toString()}")
        var yearly3Pair = parseEntryList(yearly3Response)
        yrarly3entries = yearly3Pair.first
        yearly3Tooltips = yearly3Pair.second

    }

    fun fromJson(jsonString: String): JSONArray {
        try {
            var jsonObject = JSONObject(jsonString)
            var jsonArray = (jsonObject.get("candles") as JSONArray)
            return jsonArray
        } catch (e: java.lang.Exception) {
            Timber.e(e.toString())
        }
        return JSONArray()
    }

    fun parseEntryList(jsonArray: JSONArray): Pair<ArrayList<Entry>, ArrayList<ToolTips>> {
        var entries = ArrayList<Entry>()
        var toolTips = ArrayList<ToolTips>()
        var lastFloat: Float = 0f
        var lastTime: Float = 0f
        for (i in 0..jsonArray.length()) {
            var value: Float
            var time: Float
            try {
                value = ((jsonArray.get(i) as JSONArray).get(1) as Double).toFloat()
                lastFloat = value
            } catch (e: java.lang.Exception) {
                if (lastFloat == 0f) {
                    continue
                }
                value = lastFloat
            }
            try {
                time = ((jsonArray.get(i) as JSONArray).get(0).toString()).toFloat()
                lastTime = time
            } catch (e: java.lang.Exception) {
                if (lastTime == 0f) {
                    continue
                }
                time = lastTime
            }

            entries.add(Entry(i.toFloat(), value))
            var tooltip = ToolTips(time, value)
            toolTips.add(tooltip)

        }
        return Pair(entries, toolTips)
    }

    private fun heighLightSelectedType( selectedText :TextView)
    {
        day_txt.setTextColor(resources.getColor(R.color.colorText))
        month_txt.setTextColor(resources.getColor(R.color.colorText))
        year_txt.setTextColor(resources.getColor(R.color.colorText))
        week_txt.setTextColor(resources.getColor(R.color.colorText))
        year3_txt.setTextColor(resources.getColor(R.color.colorText))
        selectedText.setTextColor(resources.getColor(R.color.colorPrimary))
    }
}