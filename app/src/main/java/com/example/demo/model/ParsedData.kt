package com.example.demo.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class ParsedData: Parcelable  {

        var yearlyLow: String? = null
        var yearlyHigh: String? = null
        var symbol: String? = null
        var highPriceRange: Double? = null
        var totalBuyQty: Double? = null
        var dayChangePerc: Double? = null
        var ltp: Double? = null
        var type: String? = null
        var tsInMillis: Int? = null
        var lowPriceRange: Double? = null
        var volume: Int? = null
        var high: Double? = null
        var low: Double? = null
        var dayChange: Double? = null
        var close: Double? = null
        var open: Double? = null
        var totalSellQty: Double? = null
        lateinit var name: String
         var body1: Double = 0.0
        var body2: Double = 0.0


}

/* class Sort : Comparator<ParsedData> {
        // Used for sorting in ascending order of
        // roll number
        override fun compare(a: ParsedData, b: ParsedData):I {
                return a.body1?.minus(b.body1!!)?: 0.0 as Double
        }
}*/

