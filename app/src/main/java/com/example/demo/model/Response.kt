package com.example.demo.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Response(

	@field:SerializedName("totalRecords")
	val totalRecords: Int? = null,

	@field:SerializedName("records")
	val records: List<RecordsItem?>? = null
) : Parcelable

@Parcelize
data class LivePriceDto(

	@field:SerializedName("symbol")
	val symbol: String? = null,

	@field:SerializedName("highPriceRange")
	val highPriceRange: Double? = null,

	@field:SerializedName("totalBuyQty")
	val totalBuyQty: Double? = null,

	@field:SerializedName("dayChangePerc")
	val dayChangePerc: Double? = null,

	@field:SerializedName("ltp")
	val ltp: Double? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("tsInMillis")
	val tsInMillis: Int? = null,

	@field:SerializedName("lowPriceRange")
	val lowPriceRange: Double? = null,

	@field:SerializedName("volume")
	val volume: Int? = null,

	@field:SerializedName("high")
	val high: Double? = null,

	@field:SerializedName("low")
	val low: Double? = null,

	@field:SerializedName("dayChange")
	val dayChange: Double? = null,

	@field:SerializedName("close")
	val close: Double? = null,

	@field:SerializedName("open")
	val open: Double? = null,

	@field:SerializedName("totalSellQty")
	val totalSellQty: Double? = null
) : Parcelable

@Parcelize
data class RecordsItem(

	@field:SerializedName("bseScriptCode")
	val bseScriptCode: Int? = null,

	@field:SerializedName("marketCap")
	val marketCap: Long? = null,

	@field:SerializedName("searchId")
	val searchId: String? = null,

	@field:SerializedName("yearlyHighPrice")
	val yearlyHighPrice: Double? = null,

	@field:SerializedName("yearlyLowPrice")
	val yearlyLowPrice: Double? = null,

	@field:SerializedName("companyName")
	val companyName: String? = null,

	@field:SerializedName("nseScriptCode")
	val nseScriptCode: String? = null,

	@field:SerializedName("closePrice")
	val closePrice: Double? = null,

	@field:SerializedName("companyShortName")
	val companyShortName: String? = null,

	@field:SerializedName("isin")
	val isin: String? = null,

	@field:SerializedName("livePriceDto")
	val livePriceDto: LivePriceDto? = null,

	@field:SerializedName("industryCode")
	val industryCode: String? = null
) : Parcelable
