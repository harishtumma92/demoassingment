package com.example.demo.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.demo.model.ParsedData
import com.example.demo.model.Response
import com.example.demo.rawjson.Jsons
import com.google.gson.Gson

class MainVM(val app: Application) : ViewModel() {

    private var stocks: Response
    private var _stockslist = MutableLiveData<List<ParsedData>>()
    val list = listOf("Market Price","52W High","52W Low","Market Cap(Cr)")
    var selecteId:Int=0


    val stockslist: LiveData<List<ParsedData>>
        get() = _stockslist


    init {
        stocks = Gson().fromJson(Jsons.stocks, Response::class.java)
        _stockslist.value=parseList()
    }

    fun refreshData(){
        var size= list.size-1
        if(selecteId==size)
        {
            selecteId=0
        }
        else {
            selecteId=selecteId+1
        }
        _stockslist.value=parseList()
    }

    class MainVMFactory(val app: Application) : ViewModelProvider.Factory {
        //val repository =repository
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return MainVM(app) as T
        }
    }

    private fun  parseList():List<ParsedData>{
        var list=ArrayList<ParsedData>()
        stocks.records?.forEach {
            var stockdata=it
            var data= ParsedData().apply {
               name=stockdata?.companyName.toString()
                when (selecteId) {
                    0 -> {
                        this.body1 = stockdata?.livePriceDto?.ltp?.toDouble() ?: 0.0
                        this.body2= stockdata?.livePriceDto?.dayChange?.toDouble() ?: 0.0
                        this.dayChangePerc= stockdata?.livePriceDto?.dayChangePerc?.toDouble() ?: 0.0
                    }
                    1 ->this. body1= stockdata?.yearlyHighPrice?.toDouble() ?: 0.0
                    2 ->this. body1= stockdata?.yearlyLowPrice?.toDouble() ?: 0.0
                    3 ->{ this.body1= (stockdata?.marketCap?.toDouble())?.div(10000000.00) ?: 0.0
                            }
                }
                //body1= stockdata?.closePrice?.toDouble() ?: 0.0
               // body1= stockdata?.yearlyHighPrice?.toDouble()?:0.0
            }
            list.add(data)
           // Collections.sort(list,Sort())
        }
        list.sortBy {
            it.body1
        }

        return  list
    }
}