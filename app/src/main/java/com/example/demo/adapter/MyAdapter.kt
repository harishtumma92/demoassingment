package com.example.demo.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.demo.GraphActivity
import com.example.demo.R
import com.example.demo.model.ParsedData
import kotlinx.android.synthetic.main.single_cell.view.*
import java.text.DecimalFormat

class MyAdapter : RecyclerView.Adapter<MyAdapter.MyViewHolder>() {
    var df = DecimalFormat("#.##")
    lateinit var context: Context

    var list: ArrayList<ParsedData>
    var isPercentShow = false

    init {
        list = ArrayList<ParsedData>()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        context = parent.context
        val iteamView =
            LayoutInflater.from(parent.context).inflate(R.layout.single_cell, parent, false)
        return MyViewHolder(iteamView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.titleTxt.text = list.get(position).name
        holder.body1Txt.text = df.format(list.get(position).body1)
        if (isPercentShow) {
            holder.body2Txt.visibility = View.VISIBLE
            holder.body2Txt.text =
                df.format(list.get(position).body2) + " (${df.format(list.get(position).dayChangePerc)}%)"
            if (list.get(position).body2 < 0) {
                holder.body2Txt.setTextColor(context.resources.getColor(R.color.colorRed))
            } else {
                holder.body2Txt.setTextColor(context.resources.getColor(R.color.colorPrimary))
            }

        } else {
            holder.body2Txt.visibility = View.GONE
        }

        holder.layout.setOnClickListener {
            var i=Intent(context,GraphActivity::class.java)
            context.startActivity(i)
        }

    }

    fun setData(list: List<ParsedData>) {
        this.list.clear()
        this.list.addAll(list)
        this.notifyDataSetChanged()
    }

    class MyViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val titleTxt = v.txt_name
        val body1Txt = v.txt_body1
        val body2Txt = v.txt_body2
        val layout = v.stock_layout
    }

}