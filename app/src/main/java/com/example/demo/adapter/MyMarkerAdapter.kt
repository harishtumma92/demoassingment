package com.example.demo.adapter

import android.content.Context
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import kotlinx.android.synthetic.main.marker_layout.view.*
import timber.log.Timber
import java.lang.Exception
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class MyMarkerAdapter(context: Context, layoutResourse: Int) : MarkerView(context, layoutResourse) {
    lateinit var toolTipsList: List<ToolTips>
     var showTimeOnly:Boolean = false

    constructor(context: Context, layoutResourse: Int, toolTipsList: List<ToolTips>, showTimeOnly:Boolean) : this(
        context,
        layoutResourse
    ) {
        this.showTimeOnly=showTimeOnly
        this.toolTipsList = toolTipsList
    }

    override fun refreshContent(e: Entry?, highlight: Highlight?) {
        value_txt.setText(e?.y.toString())
        var pos = e?.x?.toInt()
        if (pos != null) {
            try {
                var day = toolTipsList.get(pos).dateSec
                date_txt.text = parsedDateSec(day) + " , "
            }
            catch (e:Exception)
            {
                date_txt.text = " , "
            }
        } else {
            date_txt.setText(" , ")
        }
        super.refreshContent(e, highlight)
    }

    private fun parsedDateSec(sec:Float):String
    {
        try {
            var milis=( sec*1000 ).toLong()
            val date = Date(milis)
            val sdf:DateFormat
            if(showTimeOnly)
            {
                sdf = SimpleDateFormat("h:mm,a")
            }
            else{
                sdf  = SimpleDateFormat("EEEE,MMMM d,yyyy h:mm,a")
            }
            return sdf.format(date)

        }
        catch (e:Exception)
        {
            Timber.e("parsedDateSec :${e.toString()}")
        }
        return "-"
     }
/*

    override fun getOffset(): MPPointF {
        return super.getOffset()

    }
*/
}

data class ToolTips(
    var dateSec: Float = 0.0f,
    var value: Float = 0.0f
)